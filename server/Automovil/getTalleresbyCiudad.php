<?php 
header('Access-Controll-Allow-Origin:http://localhost:4200');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include("bd.php");

$params = json_decode(file_get_contents("php://input"));

$sql = "SELECT  
                 P.TIPOID AS TIPOID, P.NUMID AS NUMID, T.NOMTER AS NOMBRE, INITCAP(T.DIREC) AS DIRECCION, INITCAP(T.URBANIZACION) AS URBANIZACION,INITCAP(T.VIAPRINCIPAL) AS VIAPRINCIPAL,INITCAP(T.REFERENCIA) AS REFERENCIA, t.LOCALPISO AS LOCALPISO, INITCAP(E.DESCESTADO) AS ESTADO, INITCAP(C.DESCCIUDAD) AS CIUDAD, T.TELEF1 AS TELEFONO1, T.TELEF2 AS TELEFONO2,LOWER(T.EMAIL) AS EMAIL
                    FROM 
                        PROVEEDOR P 
                    JOIN 
                        LVAL L 
                    ON 
                        P.TIPOPROVEEDOR=L.CODLVAL 
                    JOIN 
                        TERCERO T 
                    ON 
                        P.TIPOID = T.TIPOID 
                        AND P.NUMID = T.NUMID 
                    JOIN 
                        ESTADO E
                    ON
                        T.CODESTADO = E.CODESTADO
                        AND T.CODPAIS = E.CODPAIS
                    JOIN 
                        CIUDAD C
                    ON
                        C.CODCIUDAD = T.CODCIUDAD
                        AND C.CODESTADO = T.CODESTADO
                        AND C.CODPAIS = T.CODPAIS
                    WHERE 	
                        L.TIPOLVAL='TIPOPRVE' 
                        AND (L.CODLVAL = '01' OR L.CODLVAL = '04')
                        AND P.AFILIACION = 'S'	
                        AND P.STSPROVEEDOR = 'ACT'
                        AND T.CODPAIS = '001'
                        AND T.CODESTADO = '$params->estado'
                        AND T.CODCIUDAD = '$params->ciudad'
                    ORDER BY ESTADO";

    $result = $conn->query($sql);
    $ciudad = $result->fetch_all(MYSQLI_ASSOC);
    echo json_encode($ciudad);
?> 